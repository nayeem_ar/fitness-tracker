export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBexROSh8IzgS9BlGqpU04HfjAxs73hOfI",
    authDomain: "ng-fitness-tracker-31419.firebaseapp.com",
    databaseURL: "https://ng-fitness-tracker-31419.firebaseio.com",
    projectId: "ng-fitness-tracker-31419",
    storageBucket: "ng-fitness-tracker-31419.appspot.com",
    messagingSenderId: "682988523742",
    appId: "1:682988523742:web:5f0228aca9dccfe975324f"
  }
};
