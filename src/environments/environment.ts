// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBexROSh8IzgS9BlGqpU04HfjAxs73hOfI",
    authDomain: "ng-fitness-tracker-31419.firebaseapp.com",
    databaseURL: "https://ng-fitness-tracker-31419.firebaseio.com",
    projectId: "ng-fitness-tracker-31419",
    storageBucket: "ng-fitness-tracker-31419.appspot.com",
    messagingSenderId: "682988523742",
    appId: "1:682988523742:web:5f0228aca9dccfe975324f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
